
function [eq,gradeq] = InverseModeForwBack3RFSeqn(guess,geometry,Kee,L,wp,wd,qpd,Nf)

Ci = [zeros(1,2);eye(2)];


beameq = zeros(3*Nf,1);
closureeq = zeros(3*2,1); 
dbeameqdqa = zeros(3*Nf,2);
dbeameqdqe = zeros(3*Nf,3*Nf);
dbeamdeqdlamb = zeros(3*Nf,3*2);
dwrenchdqa = zeros(3,3);
dwrenchdqe = zeros(3,3*Nf);
dwrencheqdqp = zeros(3,3);
dwrenchdlamb = zeros(3,3*2);
dclosurdqa = zeros(3*2,3);
dclosurdqe = zeros(3*2,3*Nf);
dclosurdqp = zeros(3*2,3);

qa = guess(1:3,1);
qe = guess(1+3:3+3*Nf,1);
qp = guess(1+3+3*Nf:3+3*Nf+3,1);
lambda = guess(1+3+3*Nf+3:3+3*Nf+3+3*2,1);

pplat = qp(1:2);
thplat = qp(3);

wrencheq = wp;

basepoints = geometry.basepoints;
platformpoints = geometry.platformpoints;

for i = 1:3
    % extract variables
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    lambdai = lambda(1+2*(i-1):2*i,1);
    p0 = basepoints(:,i);
    ppoint = platformpoints(:,i);
    th0 = qai;
    
    %% FORWARD INTEGRATION: Geometry
    % initial value at s = 0
    y01 = [p0;th0;zeros(2+2*Nf,1);1;zeros(Nf,1)];
    
    % integration
    funforward = @(s,y) OdefunAssumedForward(s,y,qei,Nf);
    [~,y] = ode45(funforward,[0,L],y01);
    ygeom = y(end,:)';

    % extract results
    pL = ygeom(1:2,1);
    thL = ygeom(3,1);
    dpdqaL = ygeom(4:5,1);
    dpxdqeL = ygeom(1+5:5+Nf,1);
    dpydqeL = ygeom(1+5+Nf:5+2*Nf,1);
    dpdqeL = [dpxdqeL';dpydqeL'];
    dthdqaL = ygeom(1+5+2*Nf,1); 
    dthdqeL = ygeom(1+5+2*Nf+1:end,1)';     
    
    % wrench on local - tip frame. 
    wrench = (Ci*lambdai);


    %% BACKWARD INTEGRATION: Loads
    % initial values at s = L
  
    y02 = [wrench;zeros(Nf,1);...
           zeros(3+Nf,1);...
           zeros(3*Nf+Nf*Nf,1);...
           reshape(eye(3),3*3,1);zeros(3*Nf,1)];

    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf);
    [~,y] = ode45(funbackward,[L,0],y02);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+3:3+Nf,1); 
    dQcdqai = yforces(1+3+Nf+3:3+Nf+3+Nf,1);
    dQcdqei = reshape(yforces(1+3+Nf+3+Nf+3*Nf:3+Nf+3+Nf+3*Nf+Nf*Nf,1),Nf,Nf);
    dQcdw0 = reshape(yforces(1+3+Nf+3+Nf+3*Nf+Nf*Nf+3*3:3+Nf+3+Nf+3*Nf+Nf*Nf+3*3+3*Nf,1),Nf,3);
    dQcdlamb = dQcdw0 * Ci; 
      
    %% EQUATIONS
    % beam equations and gradient components IN LOCAL FRAME
    beameq(1+Nf*(i-1):Nf*i,1) = Kee*qei+Qc;
    dbeameqdqa(1+Nf*(i-1):Nf*i,i) = dQcdqai;
    dbeameqdqe(1+Nf*(i-1):Nf*i,1+Nf*(i-1):Nf*i) = Kee + dQcdqei;
    dbeamdeqdlamb(1+Nf*(i-1):Nf*i,1+2*(i-1):2*i) = dQcdlamb;
    
    %------------------------------------------
    % closure loop equations and gradient components
    closureeq(1+2*(i-1):2*i,1) = pL-(pplat+Rz(thplat)*ppoint);
    dclosurdqa(1+2*(i-1):2*i,i) = dpdqaL;
    dclosurdqe(1+2*(i-1):2*i,1+Nf*(i-1):Nf*i) = dpdqeL;
    dclosurdqp(1+2*(i-1):2*i,:) = -[eye(2),Rz(thplat+pi/2)*ppoint];
   
    %------------------------------------------
    % contribution to equilibrium and gradient components in GLOBAL FRAME
    globalforce = Rz(thL)*wrench(2:3);
    globalpoint = Rz(thplat)*ppoint;
    forterm = globalforce;
    momterm = wrench(1) + (globalpoint(1)*globalforce(2)-globalpoint(2)*globalforce(1));
    
    wrencheq = wrencheq - [momterm;forterm]; 
    
    dglobalforcedthL = Rz(thL+pi/2)*wrench(2:3);
    dwrenchdqa(:,i) = -[(+globalpoint(1)*dglobalforcedthL(2)-globalpoint(2)*dglobalforcedthL(1));dglobalforcedthL]*dthdqaL;
    dwrenchdqe(:,1+Nf*(i-1):Nf*i) = -[(+globalpoint(1)*dglobalforcedthL(2)-globalpoint(2)*dglobalforcedthL(1));dglobalforcedthL]*dthdqeL;
    dglobalpointdthp = Rz(thplat+pi/2)*ppoint;
    dwrencheqdqp = dwrencheqdqp - [zeros(1,2),(dglobalpointdthp(1)*globalforce(2)-dglobalpointdthp(2)*globalforce(1));zeros(2,3)];
    dglobalforcedlamb = Rz(thL)*eye(2);
    dwrenchdlamb(:,1+2*(i-1):2*i) = - [(globalpoint(1)*dglobalforcedlamb(2,:)-globalpoint(2)*dglobalforcedlamb(1,:));dglobalforcedlamb];
end

%---------------------------------------
% inverse problem equations
inveq = qp-qpd;

%----------------------------------------
% equations and gradient wrt variables
eq = [beameq;wrencheq;closureeq;inveq;];
gradeq = [dbeameqdqa, dbeameqdqe, zeros(3*Nf,3), dbeamdeqdlamb;
          dwrenchdqa, dwrenchdqe, dwrencheqdqp, dwrenchdlamb;
          dclosurdqa, dclosurdqe, dclosurdqp, zeros(3*2,3*2);
          zeros(3,3), zeros(3,3*Nf), eye(3), zeros(3,3*2);];

end  