function yd = OdefunAssumedForward(s,y,qei,Nf)

th = y(3);
dthdqa = y(1+2+1+2+2*Nf,1); 
dthdqe = y(1+2+1+2+2*Nf+1:end,1); 
b = BaseFcnLegendre(s,Nf);

pxd = cos(th);
pyd = sin(th);
thd = b'*qei;

dpxdqad = -sin(th)*dthdqa;
dpydqad = +cos(th)*dthdqa;
dpxdqed = -sin(th)*dthdqe;
dpydqed = +cos(th)*dthdqe;
dthdqad = 0;
dthdqed = b;

yd = [pxd;pyd;thd;dpxdqad;dpydqad;dpxdqed;dpydqed;dthdqad;dthdqed];

end