%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% INPUT FILE

%% Simulation parameters
% to obtain fixed grid behaviour, set stepsize = stepmin
guessflag = 'C'; %choose between K (bestkantorovich) or C (bestconditioning)

maxiter = 10;
TOL = 10^-4;
TOL2 = 180;
stepsize_x = 0.01;
stepsize_y = 0.01;
boxsize = [-1 +1 -1 +1 -180*pi/180 180*pi/180];
n_th = 360;
thstep = (boxsize(6)-boxsize(5))/n_th;
n_rad = 2^3;
tau = 200;

%% STORE VARIABLES

params.n_rad = n_rad;
params.stepsize_x = stepsize_x;
params.stepsize_y = stepsize_y;
params.stepsize_z = thstep;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.guessflag = guessflag;
params.tau = tau;

