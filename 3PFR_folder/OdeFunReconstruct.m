function yd = OdeFunReconstruct(s,y,qei,Nf)

th = y(3);
b = BaseFcnLegendre(s,Nf);

pxd = cos(th);
pyd = sin(th);
thd = b'*qei;



yd = [pxd;pyd;thd];
end