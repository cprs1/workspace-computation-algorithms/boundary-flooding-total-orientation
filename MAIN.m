%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
% FILE_3RFRrobotfile
% FILE_3PFRrobotfile
FILE_3PFRrobotfile_2
% FILE_2RFRFrobotfile

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 0; % set 1 to see how it works during computation
[WK,outstruct] = FCN_BoundaryFloodingSpatialWK(fcn,params,instantplot);

%% 3D plot

volumetot = FCN_plot_space(WK,params)

%% Slice

th_coord = -90*pi/180
str = 'XY';

PlotSlice(WK,str,th_coord)

%% Computation of orientability
OrientationPlot(WK,params)
