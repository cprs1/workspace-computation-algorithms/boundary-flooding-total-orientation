%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to obtain orientability plot
% INPUT:
% WK_full: array with workspace points
% parameters: structure with simulation parameters


function OrientationPlot(WK_full,parameters)

n_rad = parameters.n_rad;

th_vect = unique(WK_full(:,4));
n_thslices = numel(th_vect);
idslice = 4;
id1 = 2;
id2 = 3;

areap = zeros(n_rad,1);
gridd = definePlanarGrid(parameters);
np = numel(gridd(:,1));
fill_WK = [(1:1:np)',gridd,zeros(np,1)];


for j = 1:n_thslices
    slice_coordinate = th_vect(j);
    coord_values = WK_full(:,4);
    [~,id] = min(abs(coord_values-slice_coordinate));
    correct_coordinate = WK_full(id,idslice);
    idok = (WK_full(:,idslice) == correct_coordinate);
    WK = WK_full(idok==1,:);
    polyshapes = cell(n_rad,1);

    for i = 1:n_rad
        idwk_i = (WK(:,5) == 1 & WK(:,6) == i );
        points = [WK(idwk_i,id1),WK(idwk_i,id2)];
        [k,areap(i)] = boundary(points(:,1),points(:,2),.9);
        if numel(k)>1
            polyshapes{i,1} = polyshape(points(k,1),points(k,2));
        end
    end

    maxarea = max(areap);
    tmp_col = zeros(np,1);

    for i = 1:n_rad
        if areap(i)>=0.05*maxarea & areap(i)>0
           TFin = isinterior(polyshapes{i},fill_WK(:,2),fill_WK(:,3));
           if areap(i)==maxarea
               tmp_col(TFin==1,1)=1;
           else
               tmp_col(TFin==1,1)=0;
           end
        end
    end

    fill_WK(:,4) = fill_WK(:,4) + tmp_col;
end

fill_WK(fill_WK(:,4)==0,4) = NaN; % to no plot

figure()
scatter(fill_WK(:,2),fill_WK(:,3),20,fill_WK(:,4),'filled')
colorbar
% a = colorbar;
% a.Label.String = 'Power (dB)';

title('Orientation')
grid on
axis equal
end